defmodule Blackjack.TableServer do

  @moduledoc """
  A supervisor for starting tables
  """

  use Supervisor
  alias Blackjack.Table

  def add_table(name) do
    Supervisor.start_child(__MODULE__, [name])
  end

  def delete_table(table) do
    Supervisor.terminate_child(__MODULE__, table)
  end

  def find_table(name) do
    Enum.find tables, fn(child) ->
      Table.name(child) == "table_#{name}"
    end
  end

  def tables do
    __MODULE__
    |> Supervisor.which_children
    |> Enum.map(fn({_, child, _, _}) -> child end)
  end

  ###
  # Supervisor API
  ###

  def start_link do
    Supervisor.start_link(__MODULE__, nil, name: __MODULE__)
  end

  def init(_) do
    children = [
      worker(Table, [], restart: :transient)
    ]

    supervise(children, strategy: :simple_one_for_one)
  end

end

