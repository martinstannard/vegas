defmodule Blackjack.Hand do

  @moduledoc """
  A hand of cards
  """
  
  use GenServer

  alias Blackjack.Counter

  def start_link do 
    GenServer.start_link(__MODULE__, nil)
  end

  @doc """
  return the list of cards for the hand
  """
  def cards(pid) do
    GenServer.call(pid, :cards)
  end

  # def count(pid) do
  #   GenServer.call(pid, :count)
  # end

  def scores(pid) do
    GenServer.call(pid, :scores)
  end

  def to_string(pid) do
    GenServer.call(pid, :to_string)
  end

  def bust?(pid) do
    GenServer.call(pid, :bust?)
  end

  def best_score(pid) do
    GenServer.call(pid, :best_score)
  end

  # @doc """
  # return the highest possible score for a hand
  # """
  # def highest_score(pid) do
  #   GenServer.call(pid, :highest_score)
  # end

  @doc """
  add a card to the hand
  """
  def draw(pid, card) do
    GenServer.cast(pid, {:draw, card})
  end

  @doc """
  remove all cards from the handle_call
  """
  def clear(pid) do
    GenServer.cast(pid, :clear)
  end

  ###
  # Server
  ###

  def init(_) do
    {:ok, []}
  end

  def handle_call(:cards, _from, cards) do
    {:reply, cards_to_array(cards), cards}
  end

  def handle_call(:count, _from, cards) do
    {:reply, length(cards), cards}
  end

  def handle_call(:scores, _from, cards) do
    {:reply, Counter.scores(cards), cards}
  end

  def handle_call(:to_string, _from, cards) do
    s = cards
    |> Enum.map(fn(c) -> elem(c, 0) end)
    |> Enum.reverse
    |> Enum.join("-")
    |> String.ljust(20)
    {:reply, s <> " [" <> Integer.to_string(Counter.best_score(cards)) <> "]", cards}
  end

  def handle_call(:bust?, _from, cards) do
    {:reply, Counter.best_score(cards) < 0, cards}
  end

  def handle_call(:best_score, _from, cards) do
    {:reply, Counter.best_score(cards), cards}
  end

  # def handle_call(:highest_score, _from, cards) do
  #   {:reply, Counter.highest_score(cards), cards}
  # end

  def handle_cast({:draw, card}, cards) do
    {:noreply, Enum.reverse([card | Enum.reverse(cards)])}
  end

  def handle_cast(:clear, _) do
    {:noreply, []}
  end

  defp cards_to_array(cards) do
    cards
    |> Enum.map(fn (c) -> card_to_hash c end)
  end

  defp card_to_hash({rank, suit}) do
    %{rank: rank, suit: suit}
  end

end
