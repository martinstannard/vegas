defmodule Blackjack.Counter do

  @moduledoc """
  provides scoring functions for  a hand of cards
  """

  @doc """
  takes a hand of cards and returns a list of possible scores, taking into account
  that Aces may be scored as 1 or 11
  """
  def scores(cards) do
    cards
    |> Enum.map(fn(c) -> card_score(elem(c, 0)) end)
    |> Enum.reduce([0], fn(card_scores, acc) -> add_card(acc, card_scores) end)
    |> Enum.uniq
  end

  @doc """
  calculates the best score for hand, 21 or less
  """
  def best_score(cards) do
    cards
    |> scores
    |> Enum.filter(fn(s) -> s < 22 end)
    |> best_or_bust
  end

  @doc """
  calculates the highest score for a hand (may be > 21)
  """
  def highest_score(cards) do
    cards
    |> scores
    |> Enum.max
  end

  defp card_score(card) do
    cond do
      card == "Ace" -> [1, 11]
      Enum.member?(~w[Jack Queen King], card) -> [10] 
      true -> [String.to_integer(card)]
    end
  end

  defp add_card(current_scores, card) do
    for i <- current_scores, j <- card, do: i + j
  end

  defp best_or_bust([]), do: -1
  defp best_or_bust(list) do
    list
    |> Enum.max
  end

end
