defmodule Blackjack.Player do

  @moduledoc """
  A player has a Hand
  """

  use GenServer

  alias Blackjack.{Hand,Player}

  def start_link(player_name) do
    GenServer.start_link(__MODULE__, player_name)
  end

  def hand(pid) do
    GenServer.call(pid, :hand)
  end

  def name(pid) do
    GenServer.call(pid, :name)
  end

  def move(pid) do
    GenServer.call(pid, :move)
  end

  @doc """
  prints the players current hand to the console
  """
  def print(pid) do
    GenServer.call(pid, :print)
  end

  @doc """
  has the player busted?
  """
  def bust?(pid) do
    GenServer.call(pid, :bust?)
  end

  @doc """
  what is the best score <= 21
  """
  def best_score(pid) do
    GenServer.call(pid, :best_score)
  end

  @doc """
  add a card to the player's hand
  """
  def draw(pid, card) do
    GenServer.cast(pid, %{draw: card})
  end

  @doc """
  clears the cards from the player's hand
  """
  def clear(pid) do
    GenServer.cast(pid, :clear)
  end

  @doc """
  returns the number of credits for the player
  """
  def credits(pid) do
    GenServer.call(pid, :credits)
  end

  @doc """
  changes the player's credits by amount
  """
  def transact(pid, amount) do
    GenServer.cast(pid, {:transact, amount})
  end

  @doc """
  returns a representation of the player for use in the elm app
  """
  def get_state(pid) do
    GenServer.call(pid, :get_state)
  end

  def result(pid, dealer) do
    GenServer.call(pid, {:result, dealer})
  end

  ###
  # Server
  ###

  def init(player_name) do
    {:ok, hand} = Hand.start_link
    {:ok, %{hand: hand, name: player_name, status: "none", credits: 0}}
  end

  def handle_call(:get_state, _from, state) do
    player_state = %{
      name: state.name,
      credits: state.credits,
      status: state.status,
      hand: %{
        cards: Hand.cards(state.hand),
        score: Hand.best_score(state.hand)
      }
    }
    {:reply, player_state, state}
  end

  def handle_call(:hand, _from, state) do
    {:reply, state.hand, state}
  end

  def handle_call(:credits, _from, state) do
    {:reply, state.credits, state}
  end

  def handle_call(:name, _from, state) do
    {:reply, state.name, state}
  end

  def handle_call(:move, _from, state) do
    scores = Hand.scores(state.hand)
    outcome = cond do
      Enum.min(scores) > 21 -> :bust
      Enum.min(scores) >= 20 -> :stand
      Enum.min(scores) < 16 -> :hit
      true -> :stand
    end
    {:reply, outcome, state}
  end

  def handle_call(:print, _from, state) do
    IO.puts String.ljust(state.name, 20) <> " " <> Hand.to_string(state.hand)
    {:reply, state, state}
  end

  def handle_call(:bust?, _from, state) do
    {:reply, Hand.bust?(state.hand), state}
  end

  def handle_call(:best_score, _from, state) do
    {:reply, Hand.best_score(state.hand), state}
  end

  def handle_call({:result, dealer}, _from, state) do
    score = Hand.best_score(state.hand)
    dealer_score = Player.best_score(dealer)
    hand_state = cond do
      Hand.bust?(state.hand) -> "#f7a0a0"
      Player.bust?(dealer) -> "#adf7a0"
      dealer_score < score -> "#adf7a0"
      dealer_score > score -> "#f7a0a0"
      true -> "#f7f5a0"
    end
    {:reply, :ok, Map.put(state, :status, hand_state)}
  end

  def handle_cast(%{draw: card}, state) do
    Hand.draw(state.hand, card)
    {:noreply, state}
  end

  def handle_cast({:transact, amount}, state) do
    {:noreply, Map.put(state, :credits, state.credits + amount)}
  end

  def handle_cast(:clear, state) do
    Hand.clear(state.hand)
    {:noreply, Map.put(state, :status, "none")}
  end

end
