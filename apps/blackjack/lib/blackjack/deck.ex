defmodule Blackjack.Deck do

  @moduledoc """
  provides functionality for a deck of cards
  """

  use GenServer

  @suit ~w[Hearts Clubs Diamonds Spades]
  @rank ~w[Ace 2 3 4 5 6 7 8 9 10 Jack Queen King]

  def start_link do
    GenServer.start_link(__MODULE__, nil)
  end

  def hit(pid) do
    GenServer.call(pid, :hit)
  end

  def count(pid) do
    GenServer.call(pid, :count)
  end

  def new(pid) do
    GenServer.cast(pid, :new)
  end

  def init (_) do
    {:ok, new_deck}
  end

  def handle_call(:hit, _from, [card|deck]) do
    if length(deck) == 0 do
      deck = new_deck 
    end
    {:reply, card, deck}
  end

  def handle_call(:count, _from, deck) do
    {:reply, length(deck), deck}
  end

  def handle_cast(:new, _) do
    {:noreply, new_deck}
  end

  defp new_deck do
    cards = for i <- @rank, j <- @suit, do: {i, j}
    Enum.shuffle(cards)
  end

end
