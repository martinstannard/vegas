defmodule Blackjack.DeckTest do

  alias Blackjack.Deck

  use ExUnit.Case, async: true

  setup do
    {:ok, deck} = Deck.start_link
    {:ok, deck: deck}
  end

  test "can be started", %{deck: deck} do
    assert deck
  end

  test "initially has 52 cards", %{deck: deck} do
    Deck.start_link
    assert Deck.count(deck) == 52
  end

  test "can deal a card", %{deck: deck} do
    Deck.start_link
    assert tuple_size(Deck.hit(deck)) == 2
  end

  test "dealing a card removes it from the pack", %{deck: deck} do
    Deck.hit(deck)
    assert Deck.count(deck) == 51
  end

  test "can request a new deck", %{deck: deck} do
    Deck.hit(deck)
    Deck.new(deck)
    assert Deck.count(deck) == 52
  end

end

