defmodule Blackjack.TableTest do

  alias Blackjack.{Table,Player}

  use ExUnit.Case, async: true

  setup do
    {:ok, table} = Table.start_link("table")
    {:ok, player} = Player.start_link("player")
    Table.add_player(table, player)
    {:ok, table: table}
  end

  test "can be started", %{table: table} do
    assert table
  end

  test "can hit a player", %{table: table} do
    Table.hit_player table
    assert_receive :hit_player
  end

  test "can return the players cards", %{table: table} do
    cards = Table.player_cards table
    assert length(cards) == 0

    Table.hit_player table
    cards = Table.player_cards table
    assert length(cards) == 1

    Table.hit_player table
    cards = Table.player_cards table
    assert length(cards) == 2
  end

  test "can return the opponents cards", %{table: table} do
    cards = Table.opponent_cards table
    IO.puts cards
    assert length(cards) == 4
  end

end

