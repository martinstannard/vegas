module App where

import Task exposing (..)
import String exposing (..)
import Effects exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import StartApp as StartApp


app : StartApp.App Model
app =
  StartApp.start
    { init = init
    , update = update
    , view = view
    , inputs = [incomingActions]
    }


main : Signal Html
main =
  app.html


port tasks : Signal (Task Never ())
port tasks =
  app.tasks

-- MODEL

type GameRound = Play | Dealer | Tally

type alias Model =
  { player : Player
  , dealer : Player
  , opponents : List Player
  , round : GameRound
  }

type alias Player =
  { hand : Hand
  , name : String
  , credits : Int
  , status : String
  }

type alias TableHands =
  { player : Player
  , dealer : Player
  , opponents : List Player
  }


type alias Card =
  { rank : String
  , suit : String
  }


type alias Hand =
  { cards : List Card
  , score : Int
  }


type alias Cards =
  List Card


init : (Model, Effects Action)
init =
  (initialModel, Effects.none)


initialModel : Model
initialModel =
  { player = {name = "player1", credits = 0, status = "", hand = {cards =  [], score = 0}}
  , dealer = {name = "dealer", credits = 0, status = "" ,hand = {cards =  [], score = 0}}
  , opponents = []
  , round = Play
  }


-- UPDATE

type Action = SetState TableHands | Draw | Stand | NewHand | HitDealer | NoOp

update : Action -> Model -> (Model, Effects Action)
update action model =
  case action of
    NewHand ->
      (
       { model |
           round = Play
       }
      , sendMoveRequest "new")

    Draw ->
      (model, sendMoveRequest "hit")

    Stand ->
      (
        { model |
            round = Dealer
        }
      , Effects.none
      )

    HitDealer ->
      (
       { model |
           round = Tally
       }
      , sendMoveRequest "dealer")

    SetState tableHands ->
      (
        { model |
          player = tableHands.player,
          dealer = tableHands.dealer,
          opponents = tableHands.opponents
        }
        , Effects.none
      )

    NoOp ->
      (model, Effects.none)


-- VIEW
cardName : Card -> String
cardName card =
  "/images/cards/" ++ card.rank ++ "_of_" ++ (String.toLower card.suit) ++ ".png"


bust : Hand -> Bool
bust hand =
  hand.score < 0


handStyle : Player -> Attribute
handStyle player =
  case player.status of
    "none" ->
      style []
    _ -> 
      style
        [ ("background-color", player.status)]


showPlayerButtons : Hand -> GameRound -> Bool
showPlayerButtons hand round =
  bust(hand) || not(round == Play)


showDealerButton : Hand -> GameRound -> Bool
showDealerButton hand round =
  bust(hand) || round == Dealer


showNewButton : GameRound -> Bool
showNewButton round =
  round == Tally


playerScore : Player -> String
playerScore player =
  player.name ++ "       " ++ if player.hand.score >= 0 then toString(player.hand.score) else "Bust"


cardView : Int -> Card -> Html
cardView size card =
  span []
    [img [ src(cardName(card)), width size] [ ]]


opponentCard : Card -> Html
opponentCard = cardView 50


playerCard : Card -> Html
playerCard = cardView 80


playerFinished : Hand -> Bool
playerFinished hand =
  bust(hand) || False


view : Signal.Address Action -> Model -> Html
view address model =
  div []
    [ div [class "row", style [("margin-bottom", "1em")]] [
      (playerView model.player)
      , (playerView model.dealer)
    ]
    , opponentsView model.opponents
    , div [class "row"] [(buttonView address model)]
    ]


buttonView: Signal.Address Action -> Model -> Html
buttonView address model =
  div [class "col-md-12 btn-toolbar"]
    [ button [ onClick address Draw, class("btn btn-primary"),
                 disabled(showPlayerButtons model.player.hand model.round)]
        [ text "Hit me" ]
    , button [ onClick address Stand, class("btn btn-default"),
                 disabled(showPlayerButtons model.player.hand model.round)]
        [ text "Stand" ]
    , button [ onClick address HitDealer, class("btn btn-default"),
                 disabled(not(showDealerButton model.player.hand model.round))]
        [ text "Dealer" ]
    , button [ onClick address NewHand, class("btn btn-default"),
                 disabled(not(showNewButton model.round))]
        [ text "New Hand" ]
    ]


opponentHand : Player -> Html
opponentHand player =
  div [class("col-md-3 hand")]
    [ h4 [handStyle player] [text (playerScore player)]
    , div [] (List.map opponentCard player.hand.cards)
    ]


opponentsView : List Player -> Html
opponentsView opponents =
  div [class("row"), style [("margin-bottom", "1em")]]
    (List.map opponentHand opponents)


playerView : Player -> Html
playerView player =
  div [class "col-md-6 center-block hand"]
      [ h3 [handStyle player] [text (playerScore player)]
      , div [] (List.map playerCard player.hand.cards)
    ]

-- SIGNALS

-- send an action to the server
port moveRequest : Signal String
port moveRequest=
 moveRequestBox.signal

moveRequestBox : Signal.Mailbox String
moveRequestBox =
  Signal.mailbox "nil"


port tableState : Signal TableHands

incomingActions: Signal Action
incomingActions =
  Signal.map SetState tableState


-- EFFECTS

sendMoveRequest : String -> Effects Action
sendMoveRequest move =
  Signal.send moveRequestBox.address move
    |> Effects.task
    |> Effects.map (always NoOp)
