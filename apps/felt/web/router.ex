defmodule Felt.Router do
  use Felt.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Felt.Auth, repo: Felt.Repo
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Felt do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    resources "/users", UserController, only: [:index, :show, :new, :create]
    resources "/sessions", SessionController, only: [:new, :create, :delete]
  end

  scope "/tables", Felt do
    pipe_through [:browser]#, :authenticate_user]

    get "/play", TableController, :play
  end

  # Other scopes may use custom stacks.
  scope "/api", Felt do
    pipe_through :api
  end
end
