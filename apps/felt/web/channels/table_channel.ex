defmodule Felt.TableChannel do
  use Felt.Web, :channel

  alias Blackjack.Table

  def join("tables:" <> name, payload, socket) do
    IO.puts "joining table"
    if authorized?(payload) do
      find_or_create_table(name)
      {:ok, assign(socket, :table_name, name)}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  def handle_in("deal", payload, socket) do
    IO.puts "DEAL"
    find_table(socket)
    |> Table.deal
    find_table(socket)
    |> Table.bids
    push_table_state(socket)
    {:reply, {:ok, payload}, socket}
  end

  def handle_in("hit", payload, socket) do
    IO.puts "HIT"
    find_table(socket)
    |> Table.hit_player
    push_table_state(socket)
    {:reply, {:ok, payload}, socket}
  end

  def handle_in("dealer", payload, socket) do
    IO.puts "DEALER"
    find_table(socket)
    |> Table.hit_dealer
    push_table_state(socket)
    {:reply, {:ok, payload}, socket}
  end

  defp find_table(socket) do
    Blackjack.TableServer.find_table(socket.assigns[:table_name])
  end

  defp find_or_create_table(name) do
    table = Blackjack.TableServer.find_table name
    unless table do
      {:ok, _} = Blackjack.TableServer.add_table(name)
    end
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end

  defp push_table_state(socket) do
    push socket, "table_state", %{
      player: player(socket),
      dealer: dealer(socket),
      opponents: opponents(socket)
    }
  end

  defp player(socket) do
    find_table(socket)
    |> Table.player
  end

  defp dealer(socket) do
    find_table(socket)
    |> Table.dealer
  end

  defp opponents(socket) do
    find_table(socket)
    |> Table.opponents
  end

end
