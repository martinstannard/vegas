defmodule Felt.PageController do
  use Felt.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
