defmodule Felt.TableController do
  use Felt.Web, :controller

  def play(conn, _params) do
    render(conn, "play.html")
  end

end
