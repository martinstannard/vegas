// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"

// // Import local files
// //
// // Local files can be imported directly using relative
// // paths "./socket" or full ones "web/static/js/socket".

import socket from "./socket"

// Set up our Elm App
const elmDiv = document.getElementById('elm-container');
const initialState = {
    tableState:
    {
        player: { name: "", credits: 0, status: "", hand: {cards: [], score: 0}},
        dealer: { name: "", credits: 0, status: "", hand: {cards: [], score: 0}},
        opponents: []
    }
}
const elmApp = Elm.embed(Elm.App, elmDiv, initialState);

// find player data in DOM and create the corresponding channel
let element = document.getElementById("vegas")
var channel = null

if(element) {
    let userId  = element.getAttribute("data-id")
    let username = element.getAttribute("data-name")
    socket.connect()
    channel = socket.channel("tables:" + username, {})
}

if (channel) {
  channel.join()
      .receive("ok", resp => {
          channel.push("deal")
      })
    .receive("error", resp => { console.log("Unable to join", resp) })

  channel.on("table_state", data => {
    console.log("got table_state")
    console.log(data)
    elmApp.ports.tableState.send(data)
  })

  elmApp.ports.moveRequest.subscribe( move => {
    console.log("moveRequest " + move)
    switch(move) {
    case "new":
      console.log("pushing deal")
      channel.push("deal")
      break;
    case "hit":
      console.log("pushing hit")
      channel.push("hit")
      break;
    case "dealer":
      console.log("pushing dealer")
      channel.push("dealer")
      break;
    }
  })
}

export default socket

