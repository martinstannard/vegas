// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

import socket from "./socket"

// Set up our Elm App
// const elmDiv = document.getElementById('elm-container');
// const initialState = {
//     hand: {cards: [], score: 0, state: ""},
//     hit: [],
//     tableState:
//     {
//         player: { name: "", credits: 0, state: "", hand: {cards: [], score: 0}},
//         dealer: { name: "", credits: 0, state: "", hand: {cards: [], score: 0}},
//         opponents: []
//     }
// }
// const elmApp = Elm.embed(Elm.App, elmDiv, initialState);

// // Now that you are connected, you can join channels with a topic:
// let channel = socket.channel("tables:lobby", {})

// channel.join()
//     .receive("ok", resp => {
//         console.log("Joined successfully", resp)
//         channel.push("deal")
//     })
//   .receive("error", resp => { console.log("Unable to join", resp) })

// channel.on("set_hand", data => {
//   console.log("got hand", data.hand)
//   elmApp.ports.hand.send(data.hand)
// })

// channel.on("hit", data => {
//   console.log("got hit", data.cards)
//   elmApp.ports.hand.send(data.cards)
// })

// channel.on("table_state", data => {
//   console.log("got table_state")
//   console.log(data)
//   elmApp.ports.tableState.send(data)
// })

// channel.on("game_created", data => {
//   console.log("got game_created")
// })

// elmApp.ports.drawRequest.subscribe( count => {
//     console.log("drawRequest")
//     channel.push("hit")
// })

// elmApp.ports.dealRequest.subscribe( count => {
//     console.log("dealRequest")
//     channel.push("deal")
// })

// elmApp.ports.dealerRequest.subscribe( count => {
//   console.log("dealerRequest")
//   channel.push("dealer")
// })

// import Player from "./player"
let element = document.getElementById("vegas")
console.log(element)
var channel = null

if(element) {
  console.log("element found") 
    let userId  = element.getAttribute("data-id")
    let username = element.getAttribute("data-name")
    socket.connect()
  console.log("socket connected") 
    channel = socket.channel("tables:" + username, {})
    console.log(channel)
}

if(channel) {
  channel.join()
      .receive("ok", resp => {
          console.log("Joined successfully", resp)
          // channel.push("deal")
      })
    .receive("error", resp => { console.log("Unable to join", resp) })
}

export default socket
