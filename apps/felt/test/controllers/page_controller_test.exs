defmodule Felt.PageControllerTest do
  use Felt.ConnCase

  test "GET /" do
    conn = get conn(), "/"
    assert html_response(conn, 200) =~ "<div id=\"elm-container\"></div>"
  end
end
