ExUnit.start

Mix.Task.run "ecto.create", ~w(-r Felt.Repo --quiet)
Mix.Task.run "ecto.migrate", ~w(-r Felt.Repo --quiet)
Ecto.Adapters.SQL.begin_test_transaction(Felt.Repo)

